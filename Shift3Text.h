#pragma once
#include <string>
#include "PlainText.h"
#include "ShiftText.h"
using std::string;

#define CAESAR_CIPHER_KEY 3

class Shift3Text : public ShiftText 
{
public:
	Shift3Text(string text);
	~Shift3Text();
	void decrypt();

};

