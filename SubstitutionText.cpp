#include "SubstitutionText.h"
#pragma once

/*
This function is the constractor he will crate object of encrypt messege with caesar cipher
INPUT: the text to put, the key of caesar cipher
OUTPUT: the Shift Text
*/
SubstitutionText::SubstitutionText(string text, fstream & dictionary) : PlainText(text)
{
	(*_text) = encrypt(text, dictionary);
}

/*
This function is the destractor he will delete and free memory (here no need because there is no memory to free)
INPUT: nun
OUTPUT: nothing, delete evereting
*/
SubstitutionText::~SubstitutionText()
{
}

/*
This function will decrypt the encrypt text acording file
INPUT: the file
OUTPUT: void
*/
void SubstitutionText::decrypt(fstream & dictionary)
{
	string myKeys[NUM_LETTERS_ENG] = {};
	int count = 0;
	string result;
	fstream myFile;
	string line;
	string letter;
	 int index;
	
	string nawKey;
	string nawLetter;

	string lettersEng[NUM_LETTERS_ENG];

	char dot = '.';
	char comma = ',';
	char space = ' ';

	while (getline(dictionary, line))
	{
		myKeys[count] = line[PLACE_VAL];
		count++;
	
	}
	dictionary.close();

	for (int g = 0; g < NUM_LETTERS_ENG; g++)
	{
		lettersEng[g] = +MIN_SMALL_LETTER + g; //do array of al the english letters
		
	}

	
	for (int j = 0; j < (*_text).length(); j++)
	{
		
		nawLetter = (*_text)[j];
		for (int i = 0; i < NUM_LETTERS_ENG; i++)
		{			
			nawKey = myKeys[i];

			if (nawKey == nawLetter) //if we found the value
			{
				index = i;
				
				letter = lettersEng[index]; //find the key of the value
			
				result += letter;
			}
			
		}
	}

	_text = new string(result);

	_isEnc = false;


}

/*
This function will intalize notLegal variable
INPUT: true if not legal false if not
OUTPUT: void
*/
void SubstitutionText::setNotLegal(bool notLegal)
{
	_notLegal = notLegal;
}

/*
This function return notLegal variable
INPUT: nun
OUTPUT: true if not legal false if not
*/
bool SubstitutionText::getNotLegal()
{
	return _notLegal;
}

/*
This function will encrypt the text acording to file
INPUT: the text to encrypt, the file
OUTPUT: the encrypt text
*/
string SubstitutionText::encrypt(string text, fstream & dictionary)
{
	string myKeys[NUM_LETTERS_ENG] = {};
	int count=0;
	string result;
	fstream myFile;
	string line;
	string letter;

	char dot = '.';
	char comma = ',';
	char space = ' ';

	
	while (getline(dictionary, line))
	{
		myKeys[count] = line[PLACE_VAL]; //do array that place 0=a so the value of a is r...
		count++;
	}
	dictionary.close();
	

	for (int i = 0; i<text.length(); i++)
	{
		// if the letter is . , or space
		if ((*_text)[i] == dot || (*_text)[i] == comma || (*_text)[i] == space)
		{
			letter = (*_text)[i];
			result += letter;
		}
		//cheak that the letter is ~just~ a-z
		else if (!isupper((*_text)[i]) && !isdigit((*_text)[i]) && isalpha((*_text)[i]))
		{
			letter = myKeys[text[i] - MIN_SMALL_LETTER];  //regolar
			result += letter;
		}
		//if there is letters that not legal like big letter or sigh
		else
		{
			setNotLegal(true);
		}
		
	}


	_isEnc = true;

	return result;
}
