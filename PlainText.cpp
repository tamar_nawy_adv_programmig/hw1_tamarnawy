#include "PlainText.h"

/*
This function is the constractor he will crate object of base messege
INPUT: the text to put
OUTPUT: the PlainText
*/
PlainText::PlainText(string text)
{
	this->_text = new string(text);
}

/*
This function is the destractor he will delete and free memory
INPUT: nun
OUTPUT: nothing, delete evereting
*/
PlainText::~PlainText()
{
	if (this->_text)
	{
		delete this->_text;
	}
}

/*
This function will cheak if the text encrypt
INPUT: nun
OUTPUT: true if encrypt, false if not
*/
bool PlainText::isEncrypted()
{
	bool returnVal;

	if (this->_isEnc==true)
	{
		returnVal = true;
	}
	else
	{
		returnVal = false;
	}
	return returnVal;
}

/*
This function will get the text
INPUT: nun
OUTPUT: the text
*/
string PlainText::getText()
{
	return (*this->_text);
}
