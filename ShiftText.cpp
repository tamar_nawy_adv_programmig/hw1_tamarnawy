#include "ShiftText.h"

/*
This function is the constractor he will crate object of encrypt messege
INPUT: the text to put, the key to move
OUTPUT: the Shift Text
*/
ShiftText::ShiftText(string text, int key) : PlainText(text)
{
	(*_text) = encrypt(text, key);
}

/*
This function is the destractor he will delete and free memory (here no need because there is no memory to free)
INPUT: nun
OUTPUT: nothing, delete evereting
*/
ShiftText::~ShiftText()
{

}

/*
This function will decrypt the encrypt text
INPUT: the key the text encrypt
OUTPUT: void
*/
void ShiftText::decrypt(int key)
{
	string result;

	char dot = '.';
	char comma = ',';
	char space = ' ';

	char letter = 'a';
	
	
	for (int i = 0; i < (*_text).length(); i++)
	{

		// if the letter is . , or space
		if ((*_text)[i] == dot || (*_text)[i] == comma || (*_text)[i] == space)
		{
			letter = (*_text)[i];
			result += letter;
		}
		//cheak that the letter is ~just~ a-z
		else if (!isupper((*_text)[i]) && !isdigit((*_text)[i]) && isalpha((*_text)[i]))
		{
			if ((*_text)[i]>=MIN_SMALL_LETTER && (*_text)[i]<=MAX_SMALL_LETTER)
			{
				letter = (int((*_text)[i] - key - MIN_SMALL_LETTER) % NUM_LETTERS_ENG + MIN_SMALL_LETTER);

				if (letter < MIN_SMALL_LETTER)
				{
					int diffrens = MIN_SMALL_LETTER - letter;
					letter = MAX_SMALL_LETTER - diffrens + 1;
				}
			}
			else
			{
				letter = (*_text)[i];
			}

			
			result += letter;
		}
		
	}
	

	_text = new string(result);

	_isEnc = false;
}

/*
This function will intalize notLegal variable
INPUT: true if not legal false if not
OUTPUT: void
*/
void ShiftText::setNotLegal(bool notLegal)
{
	_notLegal = notLegal;
}

/*
This function return notLegal variable
INPUT: nun
OUTPUT: true if not legal false if not
*/
bool ShiftText::getNotLegal()
{
	return _notLegal;
}


/*
This function will encrypt the text acording to some key
INPUT: the text to encrypt, the key 
OUTPUT: the encrypt text
*/
//acording http://www.geeksforgeeks.org/caesar-cipher/
string ShiftText::encrypt(string text, int key)
{
	
	string result;

	char dot = '.';
	char comma = ',';
	char space = ' ';

	char letter = 'a';


	for (int i = 0; i<text.length(); i++)
	{
		// if the letter is small
		if ((*_text)[i] == dot || (*_text)[i] == comma || (*_text)[i] == space)
		{
			letter = (*_text)[i];
			result += letter;
		}
		//cheak that the letter is ~just~ a-z
		else if (!isupper((*_text)[i]) && !isdigit((*_text)[i]) && isalpha((*_text)[i]))
		{
			if (text[i]!=dot && text[i] != comma && text[i] != space)
			{
				letter = (int(text[i] + key - MIN_SMALL_LETTER) % NUM_LETTERS_ENG + MIN_SMALL_LETTER);

				if (letter < MIN_SMALL_LETTER)
				{
					int diffrens = MIN_SMALL_LETTER - letter;
					letter = MAX_SMALL_LETTER - diffrens + 1;
				}
			}
			else
			{
				letter = text[i];
			}
			
			result += letter;
		}
		//if there is letters that not legal like big letter or sigh
		else
		{
			setNotLegal(true);
		}

		
	}

	_isEnc = true; //because encrypt

	return result;
}
