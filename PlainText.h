#pragma once
#include <string>
using std::string;

class PlainText
{
public:
	PlainText(string text);
	~PlainText();
	bool isEncrypted();
	string getText();

protected:
	string* _text;
	bool _isEnc;

private:
	//
};