#include "DecryptException.h"
#include "SubstitutionText.h"
#include "Shift3Text.h"
#include "ShiftText.h"
#include "PlainText.h"
#include <iostream>

using std::cout;
using std::endl;

int main ()
{
	fstream dic;
	dic.open("dictionary.csv");

	ShiftText* theString = new ShiftText("smile bob, it is a printscreen.", 1);
	Shift3Text* theStringC = new Shift3Text("smile bob, it is a printscreen.");
	SubstitutionText* fileEnc = new SubstitutionText("smile bob, it is a printscreen.", dic);

	//string myText = theString->getText();

	cout << "my string is:" << theString->getText() << endl;
	theString->decrypt(1);
	cout << "my regolar string is:" << theString->getText() << endl;
	cout << "my string with caesar cipher is:" << theStringC->getText() << endl;
	theStringC->decrypt();
	cout << "my regolar string with caesar cipher is:" << theStringC->getText() << endl;
	cout << "my string with file is:" << fileEnc->getText() << endl;
	dic.open("dictionary.csv");
	fileEnc->decrypt(dic);
	cout << "my regolar string with file is:" << fileEnc->getText() << endl;
	cout << "is the file encrypeted?:" << fileEnc->isEncrypted() << endl;
	if (theString->getNotLegal() == true || theStringC->getNotLegal() == true || fileEnc->getNotLegal() == true)
	{
		cout << "\n\nNOTE!\nSome of the characters you entered are invalid so they were not properly encrypted" << endl;
	}
	try //trying to decrypt the second time
	{
		fileEnc->decrypt(dic);
		cout << fileEnc->getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}


	system("PAUSE");
}