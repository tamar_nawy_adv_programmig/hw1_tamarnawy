#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "PlainText.h"
#include "ShiftText.h"
#include "Shift3Text.h"
//using namespace std;
using std::string;
using std::exception;


class DecryptException : public exception
{
public:
	DecryptException();
	~DecryptException();
	string what();

private:

};
