#pragma once
#include <string>
#include <fstream>
#include "PlainText.h"
#include "ShiftText.h"
#include "Shift3Text.h"
using std::string;
using std::fstream;

#define DICTIONARY_PATH "C:\Users\magshimim\Desktop\Magshimim Home Work\11 grade\Advanced Principles\class5 15.11\HW5_TamarNawy\HW5_TamarNawy\dictionary.csv"  
#define PLACE_VAL 2

class SubstitutionText : public PlainText
{
public:
	SubstitutionText(string text, fstream& dictionary);
	~SubstitutionText();
	void decrypt(fstream& dictionary);
	void setNotLegal(bool notLegal);
	bool getNotLegal();

private:
	string encrypt(string text, fstream& dictionary);
	bool _notLegal = true;
};

