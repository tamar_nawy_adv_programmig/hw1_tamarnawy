#include "ShiftText.h"
#include "SubstitutionText.h"
#include "Shift3Text.h"
#include "DecryptException.h"
#include <iostream>

#define SHIFTING_KEY 10
#define DICTIONARY_PATH "dictionary.csv"

using std::string;
using std::cout;
using std::getline;
using std::cin;
using std::endl;

void Alice();
PlainText Bob(SubstitutionText msg);
PlainText Bob(ShiftText msg);
PlainText Bob(Shift3Text msg);


/*
This is the main function of the program.
It starts the conversation between Alice and Bob
*/
/*int main()
{
	fstream dic;
	string subMes;
	PlainText* beakMes;
	PlainText* beakMes2;
	PlainText* beakMes3;

	cout << "Alice:\nEnter a messege to send to Bob:" << endl;
	getline(cin, subMes);
	cout << "\nSending in Substitution mode..." << endl;
	dic.open(DICTIONARY_PATH);
	SubstitutionText msg2(subMes, dic);
	cout << "\n\nBob:\nThe received messege:" << endl;
	cout << msg2.getText() << endl;
	//cout << msg2.getText() << endl;
	try
	{
		dic.open(DICTIONARY_PATH);
		msg2.decrypt(dic);
		//cout << msg2.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}

	beakMes = new PlainText(Bob(msg2));

	cout << "\n\nAlice:\nReceived back:"  << beakMes->getText() << endl;
	cout << "\nSending in shift mode..." << endl;
	ShiftText msg3(subMes, SHIFTING_KEY);
	cout << "\n\nBob:\nThe received messege:" << endl;
	cout << msg3.getText() << endl;
	try
	{
		msg3.decrypt(SHIFTING_KEY);
		//cout << msg2.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}

	beakMes2 = new PlainText(Bob(msg3));
	cout << "\n\nAlice:\nReceived back:" << beakMes2->getText() << endl;
	cout << "\nSending in shift3 mode..." << endl;
	Shift3Text msg4(subMes);
	cout << "\n\nBob:\nThe received messege:" << endl;
	cout << msg4.getText() << endl;
	try
	{
		msg4.decrypt();
		//cout << msg2.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	beakMes3 = new PlainText(Bob(msg4));
	cout << "\n\nAlice:\nReceived back:" << beakMes->getText() << endl;

	system("PAUSE");
	
}*/

/*
This is Alice's part in the conversation.
It gets a legal string from the user ans sends it to Bob in
three different kinds of encryptions.
It also prints every response from Bob.
*/
void Alice()
{
	
}

/*
This function handles Bob's part of the conversation 
when he receives a message encrypted in Substitution mode
*/
PlainText Bob(SubstitutionText msg)
{
	
	cout << "\nThe decrypted messege:" << endl;
	cout << msg.getText() << endl;

	return PlainText("Thank you Alice!");
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift mode
*/
PlainText Bob(ShiftText msg)
{
	cout << "\nThe decrypted messege:" << endl;
	cout << msg.getText() << endl;
	return PlainText("Thank you again Alice!");
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift-3 mode
*/
PlainText Bob(Shift3Text msg)
{
	cout << "\nThe decrypted messege:" << endl;
	cout << msg.getText() << endl;
	return PlainText("Many Thanks Alice!");
}