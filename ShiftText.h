#pragma once
#include <string>
#include <iostream>
#include "PlainText.h"
using std::string;

#define MIN_BIG_LETTER 65
#define MAX_BIG_LETTER 90
#define MIN_SMALL_LETTER 97
#define MAX_SMALL_LETTER 122
#define NUM_LETTERS_ENG 26
#define MIN_NUM 48
#define MAX_NUM 57
#define NUM_MUMBERS 10
#define MIN_SIGN_ONE 33
#define MAX_SIGN_ONE 47
#define NUM_SIGN_ONE 15

class ShiftText : public PlainText
{
public:
	ShiftText(string text, int key);
	~ShiftText();
	void decrypt(int key);
	void setNotLegal(bool notLegal);
	bool getNotLegal();
	

private:
	string encrypt(string text, int key);
	bool _notLegal = true;

};

