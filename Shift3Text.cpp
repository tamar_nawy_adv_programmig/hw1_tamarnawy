#include "Shift3Text.h"

/*
This function is the constractor he will crate object of encrypt messege with caesar cipher
INPUT: the text to put, the key of caesar cipher
OUTPUT: the Shift Text
*/
Shift3Text::Shift3Text(string text) : ShiftText(text, CAESAR_CIPHER_KEY) 
{
	
}

/*
This function is the destractor he will delete and free memory (here no need because there is no memory to free)
INPUT: nun
OUTPUT: nothing, delete evereting
*/
Shift3Text::~Shift3Text()
{

}

/*
This function will decrypt the caesar cipher
INPUT: nun
OUTPUT: void
*/
void Shift3Text::decrypt() 
{
	ShiftText::decrypt(CAESAR_CIPHER_KEY); //call the decrypt func with key of caesar cipher
}
